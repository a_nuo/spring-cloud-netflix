package com.gupaoedu.example.orderserviceprovider;

import com.gupaoedu.springcloud.OrderService;
import com.gupaoedu.springcloud.dto.OrderDto;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.RestController;

/**
 * 咕泡学院，只为更好的你
 * 咕泡学院-Mic: 2227324689
 * http://www.gupaoedu.com
 **/
@RestController
@RefreshScope
public class OrderServiceImpl implements OrderService{

    @Value("${useLocalCache:false}")
    private boolean useLocalCache;

    @Override
    public String orders() {
        try {
//            Thread.sleep(2000);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "Return All Orders"+useLocalCache;
    }

    @Override
    public int insert(OrderDto dto) {
        return 1;
    }
}
